/**
 * Programme de conversion de SVG contenant des PNGs en SVG vectorisé
 * 
 * Ce programme est prends en entrée un fichier SVG contenant des images en PNG,
 * Les images PNG sont vectorisées en noir et blanc et remplacées dans le fichier original,
 * Le résultat est sauvegardé dans un nouveau fichier vectorisé pur.
 * 
 * Copyright (C) 2017, Aurélien VALADE
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by*
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
"use strict";
var imager = require('imagetracerjs'),
    png = require("pngjs"),
    fs = require("fs"),
    jsdom = require("jsdom");

// converti le fichier "filename" en SVG purement vectoriel.
function convert(filename, output)
{
    // Ouvre le fichier en lecture
    fs.readFile(filename, function(error, buf)
    {
        // Extrait la structure DOM du fichier XML
        var svgContent = new jsdom.JSDOM(buf);

        // Recherche les images non vectorisées
        var images = svgContent.window.document.getElementsByTagName("image");
        console.log("Found "+images.length+ " images");

        // Pour chaque image
        Array.prototype.forEach.call (images, function(img)
        {
            // Récupère les données encodées
            var attribute = img.getAttribute('xlink:href').match(/base64,(.*)/)
            // Converti les données en données binaires
            var pngBuf = Buffer.from(attribute[1], 'base64');
            // Et décompresse le PNG
            new png.PNG().parse(pngBuf, function(error, data)
            {
                // Création du SVG (texte) correspondant au PNG
                var svg = imager.imagedataToSVG({'width':data.width, 'height':data.height,
                'data':data.data}, {'lthres':0.1, 'numberofcolors':2});
        
                // Et extraction de la structure DOM correspondante
                var domImage = new jsdom.JSDOM(svg);

                // Création d'un nouveau groupe 
                var g = svgContent.window.document.createElement("g");

                // Qui sera translaté à la position de l'ancienne image
                g.setAttribute("transform", "translate("+img.getAttribute("x")+","+img.getAttribute("y")+")");

                // Puis, pour chaque chemin du SVG
                var elt = domImage.window.document.getElementsByTagName("path");

                for (var j=0;j<elt.length;j++)
                {
                    // Le chemin est ajouté au groupe, s'il est de couleur noire (les chemins blancs ne sont pas recopiés)
                    if (elt[j].getAttribute('fill') === "rgb(0,0,0)")
                    {
                        g.appendChild(elt[j]);
                    }
                }

                // Finalement, le groupe remplace l'image
                var p = img.parentNode
                p.replaceChild(g, img)

                // Et si la dernière image a été traitée,
                if (0 == images.length)
                {
                    // On récupère uniquement le contenu SVG
                    var final = svgContent.serialize().match(new RegExp(/<svg[\w\W]*\/svg>/))

                    // Et on l'écrit dans un fichier
                    fs.writeFile(output, final, function(err)
                    {
                        if (error !== null)
                            console.log("Writting file error:" +error)        
                    })
                }
            })

        });
    })
}

console.log("SVG Conversion tool\n");
if (process.argv.length>2)
{
    var filename=process.argv[2].match(/([:\/\\\w]*)([\\\/\w]*).[s|S][v|V][g|G]/);
    console.log('Trying to convert file '+filename[0]);
    console.log(filename[0] + ' ---> ' + filename[1]+filename[2]+"_output.svg")
    convert(filename[0], filename[1]+filename[2]+"_output.svg");
}