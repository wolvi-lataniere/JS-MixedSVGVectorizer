# Automatic SVG files vectorization (for PNG content)

This program automatically replaces all PNG images from an SVG file by their Black an White vectorization.

The project aims the vecorization of images for Laser engraving or cutting in the case of large mixed SVG files (composed of vector and Raster data).

The script takes all <image> tags from the original SVG and replaces them by an equivalent group of path.

This script was originally designed to vectorize ARuco codes for batch Wooden laser engraved tags creation.

# Usage

The script is composed of a single function ``convert`` taking in argument the name of the SVG file to convert and the name of the destination file.

The program can also be called as :
```
node main.js filename.svg
```

where ``filename.svg`` is the file to convert.

# Note

The script currently only support PNG encoded internal images using ``base64`` encoding.

# License

The script written by Aurélien VALADE and is licensed under GPLv3 License.